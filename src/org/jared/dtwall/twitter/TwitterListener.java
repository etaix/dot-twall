/**
 * 
 */
package org.jared.dtwall.twitter;

import twitter4j.FilterQuery;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * @author chamerling
 * 
 */
public class TwitterListener {

	public static synchronized  TwitterStream listen(String user, String password, StatusListener listener, String[] keywords) {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setUser(user);
		cb.setPassword(password);
	//	cb.setOAuthAccessToken("71339590-jR41eYbL8Ssa44iVZKkqob93iBSidcVxYeCFJkhn0");
	//	cb.setOAuthAccessTokenSecret("FjvY1gSljFGZYEqEnRyd4R590wOI6jsZSFR8rd0Eos");
		cb.setHttpRetryCount(0);
		TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
		FilterQuery query = new FilterQuery();
		query.track(keywords);
		query.setIncludeEntities(true);
		twitterStream.addListener(listener);
		twitterStream.filter(query);
		return twitterStream;
	}
}
