/**
 * 
 */
package org.jared.dtwall;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * This facade provides utility methods to manipulates images
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class ImageFacade {

	/**
	 * Draw an image into a graphic but first scale it to fit into a specific width and height
	 * 
	 * @param urlP
	 * @param maxWidth
	 * @param maxHeight
	 * @param gP
	 * @param xP
	 * @param yP
	 */
	public static void drawImage(URL imageUrlP, int maxWidth, int maxHeight, Graphics2D gP, int xP, int yP) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(imageUrlP);
			// Calculate scale
			double x_scale = (double) maxWidth / (double) img.getWidth();
			double y_scale = (double) maxHeight / (double) img.getHeight();
			// Get the min scale and apply it to width and height
			double scale = x_scale < y_scale ? x_scale : y_scale;
			int nw = (int) (img.getWidth() * scale);
			int nh = (int) (img.getHeight() * scale);
			// Create the scaled image and draw it into the Graphic
			Image scaleImage = img.getScaledInstance(nw, nh, Image.SCALE_SMOOTH);
			gP.drawImage(scaleImage, 0, 0, null);
		}
		catch (IOException e) {
			System.out.println("Error: " + e);
		}
	}

}
