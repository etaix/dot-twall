package org.jared.dtwall;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jared.dtwall.twitter.TwitterListener;

import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;

/**
 * Servlet implementation class TweetServlet
 */
public class TweetServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int IMAGE_WIDTH = 300;
	private static final int IMAGE_HEIGHT = 100;
	private static final int USER_IMAGE_WIDTH = 50;
	private static final int USER_IMAGE_HEIGHT = 50;
	private static final int IMAGE_PADDING = 10;
	private static final float FONT_SIZE = 14.0f;

	// The FIFO tweets
	private LinkedBlockingQueue<Status> tweets = new LinkedBlockingQueue<Status>();
	// The twitter stream instance
	private TwitterStream twitterStream;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		List<String> tags = new ArrayList<String>();
		tags.add("#DevoxxFR");
		tags.add("#apple");

		String login = "xxxx";
		String password = "xxxx";

		System.out.println("Creating the Tweeter listener");
		twitterStream = TwitterListener.listen(login, password, new StatusListener() {
			/**
			 * Each time a status is received, then add it to the tweet queue
			 */
			public void onStatus(Status status) {
				try {
					tweets.add(status);
					System.out.println("Tweets nb=" + tweets.size());
				}
				catch (Exception ex) {
					// Just do nothing : dropt it !
				}
			}

			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
			}

			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
			}

			public void onScrubGeo(long userId, long upToStatusId) {
			}

			public void onException(Exception ex) {
			}
		}, tags.toArray(new String[tags.size()]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	@Override
	public void destroy() {
		twitterStream.cleanUp();
		super.destroy();
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TweetServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Font font = Font.getFont("Arial");
		// font = font.deriveFont(20.0f);

		// Set the default text and try to get a real tweet
		Status status = null;
		String text = "No tweet, sorry !";
		if (tweets.size() > 0) {
			status = tweets.remove();
			text = status.getText();
		}

		// Set the image dimensions
		int width = IMAGE_WIDTH;
		int height = IMAGE_HEIGHT;

		// Create the buffered image and the graphic
		BufferedImage buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = buffer.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		Font font = g2.getFont().deriveFont(FONT_SIZE);
		Font fontBold = font.deriveFont(Font.BOLD);
		g2.setFont(font);

		// Fill the background (debug purpose only)
		g2.setPaint(Color.BLACK);
		g2.fillRect(0, 0, width, height);

		// If there's a status
		if (status != null) {
			// Draw the user profile image
			URL profileImageURL = status.getUser().getProfileImageURL();
			ImageFacade.drawImage(profileImageURL, USER_IMAGE_WIDTH, USER_IMAGE_HEIGHT, g2, 0, 0);
			
			// Get the font of a line of text in this font and render context
			g2.setFont(fontBold);
			FontMetrics metrics = g2.getFontMetrics(font);
			int unHeight = metrics.getHeight();
			// Draw the user's name
			g2.setColor(new Color(0xe7b335));
			g2.drawString(status.getUser().getName(), USER_IMAGE_WIDTH + IMAGE_PADDING, unHeight);
			// Draw the text
			g2.setFont(fontBold);
			g2.setColor(Color.WHITE);
			drawText(g2, text, g2.getFont(), USER_IMAGE_WIDTH + IMAGE_PADDING, unHeight + 2, width - USER_IMAGE_WIDTH - IMAGE_PADDING);
		}

		// Set the content type and get the output stream
		response.setContentType("image/png");
		OutputStream os = response.getOutputStream();

		// Output the image
		ImageIO.write(buffer, "png", os);
		os.close();

	}

	private static void drawText(Graphics2D g, String text, Font font, int x, int y, int width) {
		FontRenderContext frc = g.getFontRenderContext();
		// prepare font calculations
		AttributedString styledText = new AttributedString(text);
		styledText.addAttribute(TextAttribute.FONT, font);
		AttributedCharacterIterator styledTextIterator = styledText.getIterator();
		LineBreakMeasurer measurer = new LineBreakMeasurer(styledTextIterator, frc);
		// draw
		while (measurer.getPosition() < text.length()) {
			TextLayout textLayout = measurer.nextLayout(width);
			y += textLayout.getAscent();
			textLayout.draw(g, x, y);
			y += textLayout.getDescent() + textLayout.getLeading();
		}
	}

}
