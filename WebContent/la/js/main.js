/**
 * Hello
 * Three.js Particles
 * Morphing between multiple shapes
 * Getting particle coords from BMPs
 * Camera tweening
 *
 * by Felix Turner
 * www.airtight.cc
 *
 **/
if(!Detector.webgl)
	Detector.addGetWebGLMessage();

//VARS
var container, stats;
var camera, scene, renderer, particles, geometry, materials = [], parameters, i, h, color, sprite, size;
var mouseX = 0, mouseY = 0;
var stars = [];

var imageData, imageData2, imgWidth, imgHeight;

var images = []
var imageDatas = [];
var imageCount;

var particleHue;
var material;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var PARTICLE_COUNT = 10000;
var MAX_DISTANCE = 1500;
var IMAGE_SCALE = 50;

function init() {
	//load images to use as shapes
	images = loadImageArray(["img/hello.png", "img/la.png", "img/webgl.png", "img/esirius.png", "img/pjug.gif"], onImagesLoaded);
}

function onImagesLoaded() {
	imageCount = images.length;

	// stop the user getting a text cursor
	document.onselectstart = function() {
		return false;
	};
	for(var i = 0; i < images.length; i++) {
		imageDatas.push(getImgData(images[i]));
	}

	//init 3D scene
	container = document.createElement('div');
	document.body.appendChild(container);
	camera = new THREE.Camera(75, window.innerWidth / window.innerHeight, 1, 3000);
	camera.position.z = 1300;
	scene = new THREE.Scene();
	scene.fog = new THREE.FogExp2(0x000000, 0.0004);
	geometry = new THREE.Geometry();

	var sprite1 = ImageUtils.loadTexture("img/particle.png");
	material = new THREE.ParticleBasicMaterial({
		size : 130,
		map : sprite1,
		blending : THREE.AdditiveBlending,
		depthTest : false//,
	});

	for(var i = 0; i < PARTICLE_COUNT; i++) {
		geometry.vertices.push(new THREE.Vertex());
		var star = new Star();
		stars.push(star);
	}

	material.particleHue = Math.random();
	material.color = new THREE.Color(0xffffff);
	material.color.setHSV(material.particleHue, 1.0, 1.0);

	//init particle system
	particles = new THREE.ParticleSystem(geometry, material);
	particles.sortParticles = false;
	particles.rotation.x = Math.PI;
	particles.rotation.y = 0;
	particles.updateMatrix();
	scene.addObject(particles);

	//init renderer
	renderer = new THREE.WebGLRenderer({
		antialias : false,
		clearAlpha : 1,
		sortObjects : false
	});
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.sortElements = false;
	renderer.sortObjects = false;
	container.appendChild(renderer.domElement);

	//init stats
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	container.appendChild(stats.domElement);

	document.addEventListener('mousemove', onDocumentMouseMove, false);
	container.addEventListener('click', onDocumentClick, false);
	exploding = false;
	strobing = false;
	strobe = true;
	flipAngle = Math.PI;

	tweenCam();

	tweenCam2();

	animate();

}

function tweenCam2() {

	var camRange = 1000;
	var hangTime = Math.random() * 2000 + 2000;
	var camTween2 = new TWEEN.Tween(camera.position).to({
		x : getRand(-camRange, camRange),
		y : getRand(-camRange, camRange)//,
	}, hangTime).easing(TWEEN.Easing.Circular.EaseInOut).start();

	setTimeout(tweenCam2, hangTime);

}

function tweenCam() {

	if(exploding) {
		var colorTween = new TWEEN.Tween(material).to({
			particleHue : Math.random()
		}, 2600).start();
		flipAngle = flipAngle + Math.PI;

		doShapeTween();
		//setTimeout(doShapeTween,600);
		setTimeout(tweenCam, 900);

	} else {
		doShapeTween();
		var hangTime = Math.random() * 2000 + 3000;
		//time to hold on shape
		setTimeout(tweenCam, hangTime);
	}
	exploding = !exploding;
}

function doShapeTween() {

	var col = Math.random();
	for(var i = 0; i < PARTICLE_COUNT; i++) {
		//stars[i].init();
		stars[i].toggleShape();
	}

}

function onDocumentClick(event) {
	strobing = !strobing;
}

function onDocumentMouseMove(event) {
	mouseX = event.clientX - windowHalfX;
	mouseY = event.clientY - windowHalfY;
}

function animate() {
	requestAnimationFrame(animate);
	render();
	stats.update();
}

function render() {

	TWEEN.update();

	for(var i = 0; i < PARTICLE_COUNT; i++) {
		geometry.vertices[i] = new THREE.Vertex(stars[i].posn);
	}

	if(strobing) {
		material.color.setHSV(Math.random(), 1.0, strobe ? 1 : 0);
		strobe = !strobe;
	} else {
		material.color.setHSV(material.particleHue, 1.0, 1);
	}

	geometry.__dirtyVertices = true;
	renderer.render(scene, camera);
}

//do it
init();

/**
 * STAR CLASS
 */
function Star() {
	this.init();
}

Star.MAX_SPEED = 3;

Star.prototype.init = function() {

	this.toggle = false;
	this.shapeId = 1;
	this.shapePosns = [];
	this.age = 0;
	this.lifespan = 100;
	this.posn = new THREE.Vector3(0, 0, 0);
	this.randPosn = new THREE.Vector3(getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE, MAX_DISTANCE));

	for(var i = 0; i < imageCount; i++) {
		this.shapePosns.push(this.getShapePosn(imageDatas[i]));
	}

	this.gotoPosn = this.shapePosns[0];
	this.duration = 2000;
}

Star.prototype.toggleShape = function() {

	var twn = new TWEEN.Tween(this.posn).to({
		x : this.gotoPosn.x,
		y : this.gotoPosn.y,
		z : this.gotoPosn.z,
	}, this.duration).easing(TWEEN.Easing.Exponential.EaseInOut);

	twn.onComplete = this.toggleShape;

	twn.start();

	if(this.toggle) {
		this.gotoPosn = this.shapePosns[this.shapeId].clone();

		this.duration = 600;

		this.shapeId++;
		if(this.shapeId >= imageCount) {
			this.shapeId = 0;
		}
	} else {
		this.gotoPosn = this.randPosn.clone();

		this.duration = 600;
	}

	this.toggle = !this.toggle;
}

Star.prototype.getShapePosn = function(imgData) {
	//get a position based on img pixels

	var sPosn = new THREE.Vector3(getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE, MAX_DISTANCE));
	var gotIt = false;

	//give up after 3 tries
	var NUMBER_OF_ATTEMPTS = 3;
	var Z_SPREAD = 30;
	var tries = 0;
	while(gotIt == false && tries < NUMBER_OF_ATTEMPTS) {
		tries++;
		//randomly select a pixel in image data
		var imgx = Math.round(imgData.width * Math.random());
		var imgy = Math.round(imgData.height * Math.random());
		var col = getPixel(imgData, imgx, imgy);
		//read color from image
		if(col.r > 0) {
			//if npt black - set it
			sPosn = new THREE.Vector3((imgx - imgData.width / 2) * IMAGE_SCALE, (imgy - imgData.height / 2) * IMAGE_SCALE, Math.random() * Z_SPREAD * 2 - Z_SPREAD);
			gotIt = true;
		} else {
			//if black - loop
			gotIt = false;
		}
	}

	return sPosn;
}
/**
 * END STAR CLASS
 */
//UTILITY FUNCTIONS
function getPixel(imgData, x, y) {
	var r, g, b, a, offset = x * 4 + y * 4 * imgData.width;
	r = imgData.data[offset];
	g = imgData.data[offset + 1];
	b = imgData.data[offset + 2];
	a = imgData.data[offset + 3];
	//console( "rgba(" + r + "," + g + "," + b + "," + a + ")");
	var col = new THREE.Color(0xffffff);
	col.setRGB(r / 256, g / 256, b / 256);
	return col;
}

function getImgData(image) {
	var canvas = document.createElement("canvas");
	var context = canvas.getContext("2d");
	canvas.width = image.width;
	canvas.height = image.height;
	context.drawImage(image, 0, 0);
	var imgData = context.getImageData(0, 0, canvas.width, canvas.height);
	return imgData;
}

function loadImageArray(array, callback) {
	var i, l, images = [];
	images.loadCount = 0;
	for( i = 0, l = array.length; i < l; ++i) {

		images[i] = new Image();
		images[i].loaded = 0;
		images[i].onload = function() {
			images.loadCount += 1;
			this.loaded = true;

			if(images.loadCount == l && callback) {
				callback();
			}

		};
		images[i].src = array[i];

	}

	return images;
}

//function to get random number in a range
function getRand(minVal, maxVal) {
	return minVal + (Math.random() * (maxVal - minVal));
}

//////