var container, stats;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var IMPLODE_SLEEPTIME = 100;
var IMPLODE_TIME = 4000;
var EXPLODE_SLEEPTIME = 6000;
var EXPLODE_TIME = 4000;

var MAX_DISTANCE = 1500;
var IMAGE_SCALE = 8;
var PARTICLE_COUNT = 15000;
var PIXEL_SIZE = 16;
var camera, scene, renderer, geometry;
var imageDatas = [];
var imageCount = 0;
var pixels = [];
var images;

// Let's start
init();

/**
 * Initialize the system:
 * - Verify if the browser is WegGL compliant
 * - loads static images
 * The particles system will really start when the static images will be loaded (the callback will be called, then the system starts)
 */
function init() {
	// First verify if the browser is WebGL compliant, otherwise display a message
	if (!Detector.webgl) {
		Detector.addGetWebGLMessage();
	}
	// Then stop the user getting a text cursor
	document.onselectstart = function() {
		return false;
	};

	// Initialize static imagesThe callback will be called when images will be downloaded
	images = loadImageArray([ "tweets/devoxx-france-2.png"], onImagesLoaded);
}

/**
 * Callback function called when static images are loaded
 */
function onImagesLoaded() {
	imageCount = images.length;
	for ( var i = 0; i < images.length; i++) {
		imageDatas.push(getImgData(images[i]));
	}

	container = document.createElement('div');
	document.body.appendChild(container);

	// Create the camera, the scene and the fog
	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 3000);
	camera.position.z = 1000;
	scene = new THREE.Scene();
	//scene.fog = new THREE.FogExp2(0x000000, 0.0004);

	// Create vertices and Pixels: one verticle per pixel. Also build the vertex colors array
	var vertexColors = [];
	geometry = new THREE.Geometry();
	for ( var i = 0; i < PARTICLE_COUNT; i++) {
		var vector = new THREE.Vector3(Math.random() * 2000 - 1000, Math.random() * 2000 - 1000, Math.random() * 2000 - 1000);
		geometry.vertices.push(new THREE.Vertex(vector));
		vertexColors[i] = new THREE.Color();
		vertexColors[i].setHSV(Math.random(), 1.0, 1.0);

		var pixel = new Pixel(i);
		pixels.push(pixel);
	}
	geometry.colors = vertexColors;

	for ( var i = 0; i < imageCount; i++) {
		addImage(imageDatas[i]);
	}

	// Create the material and set the vertexColors to true => WebGL will use the geometry.colors instead of the material's color
	var material = new THREE.ParticleBasicMaterial({
		size : PIXEL_SIZE,
		map : THREE.ImageUtils.loadTexture("images/particle1.png"),
		blending : THREE.AdditiveBlending,
		depthTest : false,
		vertexColors : true,
		transparent : false
	});

	// Create the particle system
	var particles = new THREE.ParticleSystem(geometry, material);
	// particles.sortParticles = true;
	// particles.rotation.x = Math.random() * 6;
	// particles.rotation.y = Math.random() * 6;
	// particles.rotation.z = Math.random() * 6;

	scene.add(particles);

	// Create the WebGL renderer and add it to the container
	renderer = new THREE.WebGLRenderer({
		clearAlpha : 0
	});
	renderer.setSize(window.innerWidth, window.innerHeight);
	container.appendChild(renderer.domElement);
	container.style.position = 'absolute';
	container.style.top = '0px';
	container.style.zIndex = '-1';

	// Add the FPS state
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	document.body.appendChild(stats.domElement);

	// Add listeners
	document.addEventListener('mousemove', onDocumentMouseMove, false);
	document.addEventListener('click', onDocumentClick, false);

	// Start the camera animation
	animateCamera();
	// Show the first image
	showNextImage();
	animate();

}

/**
 * Change the camera position randomly
 */
function animateCamera() {
	var camRange = 1000;
	var hangTime = 3000;
	var camTween = new TWEEN.Tween(camera.position).to({
		x : getRand(-camRange, camRange),
		y : getRand(-camRange, camRange)
//	 , z : 0
	 , z : getRand(2*camRange/3, camRange*2)
	}, hangTime).easing(TWEEN.Easing.Circular.EaseInOut).start().onComplete(function() {
		animateCamera();
	});
}

function onDocumentClick(event) {
	showNextImage();
}

function onDocumentMouseMove(event) {
	mouseX = event.clientX - windowHalfX;
	mouseY = event.clientY - windowHalfY;
}

//
/**
 * Animate the scene: - request another animation frame - render the scene - render the stats
 */
function animate() {
	requestAnimationFrame(animate);
	render();
	stats.update();
}

/**
 * Render the scene
 */
function render() {
	// Update all tweens
	TWEEN.update();

	// Change the camera position, but look at the scene (x:0, y:0, z:0)
	camera.position.x += (mouseX - camera.position.x) * 0.05;
	camera.position.y += (-mouseY - camera.position.y) * 0.05;
	camera.lookAt(scene.position);

	// Update verticles position and color
	for ( var i = 0; i < PARTICLE_COUNT; i++) {
		geometry.vertices[i].position.x = pixels[i].currentPos.x;
		geometry.vertices[i].position.y = pixels[i].currentPos.y;
		geometry.vertices[i].position.z = pixels[i].currentPos.z;
		geometry.colors[i].setRGB(pixels[i].currentColor.r, pixels[i].currentColor.g, pixels[i].currentColor.b);
	}

	// Secret attributs to force WebGL to recalcule vertices position and colors
	geometry.__dirtyColors = true;
	geometry.__dirtyVertices = true;

	// Render the scene with the camera
	renderer.render(scene, camera);
}

/**
 * Switch to the next image. This function is called each time a Pixel has finished its Tween. But the next image animation is only called when the last Pixel has finished
 */
var finishedImages = PARTICLE_COUNT;
function showNextImage() {
	if (finishedImages >= PARTICLE_COUNT) {
		for ( var i = 0; i < PARTICLE_COUNT; i++) {
			pixels[i].nextImage();
		}
		finishedImages = 0;
		// Load the next image
		loadNextImage();
	}
}

/**
 * A listener which is called when a image Pixel has finished to explode. This function add one for each explosion and try to show the next image
 */
function tweenFinishedListener() {
	finishedImages++;
	showNextImage();
}

/**
 * Add an image: For each Pixel, it determines pixel position and color according to the imgData
 * 
 * @param imgData
 */
function addImage(imgData) {
	// For each pixel: ty to determinate to correspondinf image pixel
	var pixId = 0;
	for ( var i = 0; i < PARTICLE_COUNT; i++) {
		var iPos = null;
		var iCol = null;
		var imgx = 0, imgy = 0;
		var found = false;
		while (!found && imgx < imgData.width && imgy < imgData.height) {
			imgy = Math.floor(pixId / imgData.width);
			imgx = pixId - (imgy * imgData.width);
			var col = getPixel(imgData, imgx, imgy);
			if (col.r > 0 || col.g > 0 || col.b > 0) {
				iPos = new THREE.Vector3((imgx - imgData.width / 2) * IMAGE_SCALE, -(imgy - imgData.height / 2) * IMAGE_SCALE, 0);
				iCol = new THREE.Color();
				iCol.setRGB(col.r, col.g, col.b)
				found = true;
			}
			pixId++;
		}
		// If not found then set a default value
		if (!found) {
			iPos = new THREE.Vector3(getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(0, 4 * MAX_DISTANCE));
			iCol = new THREE.Color(Math.random() * 0xFFFFFF);
		}
		// Add this pixel
		pixels[i].images.push({
			pos : iPos,
			color : iCol
		});
	}
}

function loadNextImage() {
	console.log("Adding image");
	var image = new Image();
	image.onload = function() {
		addImage(getImgData(image));
		imageCount = images.length;
		console.log("Image added");
	};
	image.src = "TweetServlet";
}

// ======================= Pixel class =====================

/**
 * The main principle of this script: - Create a fixed number of pixels - Each pixel instance is used for an image's pixel - And reused for the next image - No pixel (verticle) are
 * created during the animation phase (time and memory consuming)
 */
function Pixel() {
	this.currentImageId = 0;
	this.images = [];
	this.currentColor = new THREE.Color(Math.random() * 0xFFFFFF);
	this.explodePos = new THREE.Vector3(getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE, MAX_DISTANCE), getRand(-MAX_DISTANCE / 2, 2 * MAX_DISTANCE));
	this.currentPos = this.explodePos.clone();
}

/**
 * Initialize the pixel to the first image
 */
Pixel.prototype.init = function() {
	this.nextPos = this.images[0].pos;
	this.nextColor = this.images[0].color;
	this.duration = 2000;
}

/**
 * Switch to the next image. This function create 2 tweens: - the first one to implode to the image position and color - the second to explode to a randomly position (keep the same
 * color)
 */
Pixel.prototype.nextImage = function() {
	var self = this;
	// Get the first image values and remove it from the array
	var img = this.images.shift();

	// Create the implode tween
	var implodeTween = new TWEEN.Tween({
		x : self.currentPos.x,
		y : self.currentPos.y,
		z : self.currentPos.z,
		r : self.currentColor.r,
		g : self.currentColor.g,
		b : self.currentColor.b
	}).delay(IMPLODE_SLEEPTIME).to({
		x : img.pos.x,
		y : img.pos.y,
		z : img.pos.z,
		r : img.color.r,
		g : img.color.g,
		b : img.color.b
	}, IMPLODE_TIME).easing(TWEEN.Easing.Exponential.EaseInOut).onUpdate(function() {
		self.currentPos.x = this.x;
		self.currentPos.y = this.y;
		self.currentPos.z = this.z;
		self.currentColor.r = this.r;
		self.currentColor.g = this.g;
		self.currentColor.b = this.b;
	}).onComplete(function() {
		// Then create the explode tween without chaging the color
		var explPos = self.explodePos.clone();
		var explodeTween = new TWEEN.Tween({
			x : self.currentPos.x,
			y : self.currentPos.y,
			z : self.currentPos.z
		}).delay(EXPLODE_SLEEPTIME).to({
			x : explPos.x,
			y : explPos.y,
			z : explPos.z
		}, EXPLODE_TIME).easing(TWEEN.Easing.Exponential.EaseInOut).onUpdate(function() {
			self.currentPos.x = this.x;
			self.currentPos.y = this.y;
			self.currentPos.z = this.z;
		}).onComplete(function() {
			tweenFinishedListener();
		});
		explodeTween.start();
	});
	// Start the tween
	implodeTween.start();
}

// ==================== End pixel class ================

/**
 * Utility function which return a value between minVal and maxValue
 */
function getRand(minVal, maxVal) {
	return minVal + (Math.random() * (maxVal - minVal));
}
