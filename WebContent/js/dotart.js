/**
 * @preserve Main script for Dot-Art experience: http://dot-art.cloudfounrdy.com
 *           This file includes all functionality to render images Depends on
 *           Tween.js, Three.js libraries
 * 
 * Vendor provided code. Compressed file for the live site, this raw script at
 * http://closure-compiler.appspot.com/ using the default, "simple,"
 * optimization setting.
 * 
 * @author eric.taix@gmail.com (Eric Taix)
 */

// ---------------------------------------------------------------------------
// @see http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// ---------------------------------------------------------------------------
window.requestAnimFrame = (function() {
	return window.requestAnimationFrame || window.webkitRequestAnimationFrame
			|| window.mozRequestAnimationFrame || window.oRequestAnimationFrame
			|| window.msRequestAnimationFrame
			|| function(/* function */callback, /* DOMElement */element) {
				window.setTimeout(callback, 1000 / 60);
			};
})();

/**
 * Get window dimensions to save for later so we don't have to query it all the
 * time.
 * 
 * @return {Array.<number>} width and height of window.
 */
function getWindowDimensions() {
	return [ (window.innerWidth || document.documentElement.clientWidth),
			(window.innerHeight || document.documentElement.clientHeight) ];
}

/**
 * Reference to hold window dimensions without having to ask window for them all
 * the time.
 */
var windowDim = getWindowDimensions();

/**
 * Store the center X and Y of the window. Used when adjusting the camera and
 * rotation to make it appear to use the center of the window as the point of
 * reference.
 */
var windowHalfX = windowDim[0] / 2;
var windowHalfY = windowDim[1] / 2;

(function() {

	// Get the DOM element to attach to (assume we've got jQuery to hand)
	var $container = $('#container');
	// Create a WebGL renderer, camera and a scene
	var renderer = new THREE.WebGLRenderer();
	var scene = new THREE.Scene();
	var camera = new THREE.Camera(90, window.innerWidth / window.innerHeight,
			0, 2000);
	camera.position.z = 700;
	// Start the renderer - set the clear colour to whatever color because we
	// also set the opacity to 0.0
	renderer.setClearColor(new THREE.Color(0), 0);
	renderer.setSize(window.innerWidth, window.innerHeight);
	// Attach the render-supplied DOM element
	$container.append(renderer.domElement);

	// ================== DotBasicMaterial ==================
	/**
	 * Class DotBasicMaterial A wrapper around the Three.js
	 * ParticleBasicMaterial. This material is used for every Dot, and colors
	 * and set separately in particleSystem. The material color is used as a
	 * color filter
	 * 
	 * @param {Object}
	 *            opts Settings for DotMaterial.
	 * @param {int}
	 *            opts.color color value for material.
	 * @param {int}
	 *            opts.size of the material
	 * @param {String}
	 *            opts.texture name of the material
	 */
	function DotBasicMaterial(opts) {
		texture = (opts.texture || "images/plain64.png");
		this.material = new THREE.ParticleBasicMaterial({
			color : (opts.color || 0xFFFFFF),
			size : (opts.size || 16),
			map : THREE.ImageUtils.loadTexture(texture),
			blending : THREE.AdditiveBlending,
			vertexColors : true,
			depthTest : false,
			transparent : false
		});
	}

	// ===================== Dot ====================
	/**
	 * Class Dot A wrapper around Three.Vertex with utility methods to explode
	 * and implode. Explode position is created randomly
	 * 
	 * @param {Object}
	 *            draw The draw which owned this dot
	 * @param {Object}
	 *            opts Settings for DotParticle
	 * @param {int}
	 *            opts.x The final x value
	 * @param {int}
	 *            opts.y The final y value
	 * @param {int}
	 *            opts.z The final z value
	 * @param {int}
	 *            opts.sleepImplodeTime The delay in millisecond before
	 *            imploding
	 * @param {int}
	 *            opts.sleepExplodeTime The delay in millisecond before
	 *            exploding
	 * @param {int}
	 *            opts.explodeTime The delay in millisecond to explode
	 * @param {int}
	 *            opts.implodeTime The delay in millisecond to implode
	 */
	function Dot(opts) {
		this.target = {
			x : opts.x,
			y : opts.y,
			z : opts.z || 0
		};
		this.source = {
			x : (Math.random() * 2500) - 1250,
			y : (Math.random() * 2500) - 1250,
			z : (Math.random() * 6500) - 3000
		};
		this.sleepImplodeTime = opts.sleepImplodeTime;
		this.sleepExplodeTime = opts.sleepExplodeTime;
		this.explodeTime = opts.explodeTime;
		this.implodeTime = opts.implodeTime;
		this.explodeCallBack = (opts.explodeCallBack || function() {});
		// Create a particle
		this.particle = new THREE.Vertex(new THREE.Vector3(this.source.x,
				this.source.y, this.source.z));
		// Init tween animation for this particle
		this.initChainAnimation();
	}

	/**
	 * Initialize the TWEEN animations chain
	 */
	Dot.prototype.initChainAnimation = function() {
		this.animation = this.getImplode();
		this.animation.chain(this.getExplode());
	}

	/**
	 * Explode the Dot by using the TWEEN library.
	 */
	Dot.prototype.getExplode = function() {
		var self = this;
		var explode = new TWEEN.Tween(self.particle.position).delay(
				self.sleepExplodeTime).to({
			x : self.source.x,
			y : self.source.y,
			z : self.source.z
		}, self.explodeTime).easing(TWEEN.Easing.Exponential.EaseOut)
				.onComplete(function() {
					self.explodeCallBack();
				});
		return explode;
	}

	/**
	 * Implode the Dot by using the TWEEN library.
	 */
	Dot.prototype.getImplode = function() {
		var self = this;
		return new TWEEN.Tween(self.particle.position).delay(
				self.sleepImplodeTime).to({
			x : self.target.x,
			y : self.target.y,
			z : self.target.z
		}, self.implodeTime).easing(TWEEN.Easing.Exponential.EaseOut);
	}

	// ===================== Draw ====================
	/**
	 * Class Draw A wrapper around Three.particleSystem
	 * 
	 * @param {Object}
	 *            opts Settings for DotParticle
	 * @param {int}
	 *            opts.dotSize The size of each dot
	 * @param {int}
	 *            opts.dotPadding The padding size between each dot
	 * @param {int}
	 *            opts.sleepEnterTime The delay in millisecond before eentering
	 * @param {int}
	 *            opts.enterTime The delay in millisecond to enter
	 * @param {int}
	 *            opts.sleepImplodeTime The delay in millisecond before
	 *            imploding
	 * @param {int}
	 *            opts.implodeTime The delay in millisecond to implode
	 * @param {int}
	 *            opts.sleepExplodeTime The delay in millisecond before
	 *            exploding
	 * @param {int}
	 *            opts.explodeTime The delay in millisecond to explode
	 * @param {int}
	 *            opts.sleepExitTime The delay in millisecond before exiting
	 * @param {int}
	 *            opts.exitTime The delay in millisecond to exit
	 */
	function Draw(opts) {
		// Retrieve options
		this.dotSize = (opts.dotSize || 16);
		this.dotPadding = (opts.dotPadding || 2);
		this.sleepImplodeTime = (opts.sleepImplodeTime || 500);
		this.implodeTime = (opts.implodeTime || 2000);
		this.sleepExplodeTime = (opts.sleepExplodeTime || 3000);
		this.explodeTime = (opts.explodeTime || 1500);
		this.sleepExitTime = (opts.sleepExitTime || 3000);
		this.exitTime = (opts.exitTime || 1000);
		this.sleepEnterTime = (opts.sleepEnterTime || 0);
		this.enterTime = (opts.enterTime || 1000);
		// Create the individual particles
		this.colors = [];
		this.dots = [];
		this.geometry = new THREE.Geometry();
		this.startDot = 0;
		// Retrieve callback
		this.explodeCallBack = (opts.explodeCallBack || function() {
		});
		this.exitCallBack = (opts.exitCallBack || function() {
		});
	}

	/**
	 * A dot has been exploded
	 */
	Draw.prototype.dotExploded = function() {
		var self = this;
		self.dotCount--;
		if (self.dotCount == 0) {
			self.exit().start();
			self.explodeCallBack();
		}
	}

	/**
	 * Returns a TWEEN animation to exit the particleSystem by chaging its
	 * opacity.
	 */
	Draw.prototype.exit = function() {
		var self = this;
		return new TWEEN.Tween(self.material).delay(self.sleepExitTime).to({
			opacity : 0
		}, self.exitTime).easing(TWEEN.Easing.Circular.EaseIn).onComplete(
				function() {
					self.colors.length = 0;
					self.dots.length = 0;
					scene.removeObject(self.particleSystem);
					scene.__objectsRemoved = [];
					scene.__objectsAdded = [];					
					self.exitCallBack();
				});
	}

	/**
	 * Returns a TWEEN animation to enter the particleSystem by chaging its
	 * opacity.
	 */
	Draw.prototype.show = function() {
		var self = this;
		new TWEEN.Tween(self.material).delay(self.sleepEnterTime).to({
			opacity : 1.0
		}, self.enterTime).easing(TWEEN.Easing.Circular.EaseIn).onComplete(
				function() {
					self.animateDots();
				}).start();
	}

	/**
	 * Launch the dot animation to all dots
	 */
	Draw.prototype.animateDots = function() {
		var self = this;
		for ( var d = 0; d < self.dots.length; d++) {
			self.dots[d].animation.start();
		}
	}

	/**
	 * Load an image from a filename
	 * 
	 * @param filename
	 */
	var image = new Image();
	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');

	Draw.prototype.load = function(filename) {
		var self = this;

		// Reset dots array
		self.dots = [];
		image.onload = function() {

			canvas.width = image.width;
			canvas.height = image.height;

			context.drawImage(image, 0, 0);

			var imageData = context.getImageData(0, 0, canvas.width,
					canvas.height);
			self.decodeImageData(imageData);
		}
		image.src = filename;
	}

	var started = false;
	Draw.prototype.decodeImageData = function(imgData) {
		var self = this;
		// Now you can access pixel data from imageData.data.
		// It's a one-dimensional array of RGBA values.
		// Here's an example of how to get a pixel's color at (x,y)
		self.dotCount = 0;
		var siz = self.dotSize + self.dotPadding;
		for ( var yPos = 0; yPos < imgData.height; yPos++) {
			for ( var xPos = 0; xPos < imgData.width; xPos++) {
				var index = (yPos * imgData.width + xPos) * 4;
				var red = imgData.data[index];
				var green = imgData.data[index + 1];
				var blue = imgData.data[index + 2];
				var alpha = imgData.data[index + 3];
				if (alpha == 255) {
					dot = new Dot({
						x : xPos * siz - ((imgData.width * siz) / 2),
						y : -yPos * siz + ((imgData.height * siz) / 2),
						sleepImplodeTime : self.sleepImplodeTime,
						sleepExplodeTime : self.sleepExplodeTime,
						implodeTime : self.implodeTime,
						explodeTime : self.explodeTime,
						explodeCallBack : function() {
							self.dotExploded();
						}
					});
					self.colors[self.dotCount] = new THREE.Color();
					self.colors[self.dotCount].setRGB(red / 255, green / 255,
							blue / 255);
					// Add it to the geometry
					self.geometry.vertices.push(dot.particle);
					self.dotCount++;
					// Add the dot to the list
					self.dots.push(dot);
				}
			}
		}
		// Create the particle system
		self.material = new DotBasicMaterial({
			size : self.dotSize
		}).material;
		// Change the opacity to transparent
		self.material.opacity = 0.0;
		self.geometry.colors = self.colors;
		self.particleSystem = new THREE.ParticleSystem(self.geometry,
				self.material);
		self.particleSystem.sortParticles = true;
		scene.addObject(self.particleSystem);
		// Start the enter animation
		self.show();
		// Request the first animation frame
		if (!started) {
			requestAnimFrame(animate);
		}
	}

	// ================== SlideShow ==================
	/**
	 * Class SlideShow A Draw loader
	 * 
	 * @param {Object}
	 *            opts Settings for DotParticle
	 */
	function SlideShow(opts) {
		var self = this;
		var draws = [];
		this.nb = 0;
		image.onload = function() {

			canvas.width = image.width;
			canvas.height = image.height;

			context.drawImage(image, 0, 0);

			var imageData = context.getImageData(0, 0, canvas.width,
					canvas.height);
			self.next(imageData);
		}
		image.src = "examples/img1.png";
	}

	SlideShow.prototype.next = function(imgData) {
		var self = this;
		this.draw = null;
		if (self.nb < 1000) {
			self.nb++;
			this.draw = new Draw({
				dotSize : 50,
				dotPadding : -4,
				sleepExplodeTime : 500,
				enterTime : 500,
				explodeCallBack : function() {
					self.next(imgData);
				}
			});
			this.draw.decodeImageData(imgData);
		}
	}

	// ---------------------------------
	// Main creation
	new SlideShow();

	/**
	 * Animation loop. This function is called by requestAnimFrame function
	 */
	function animate() {
		// Update all tweens position
		TWEEN.update();
		// Update the camera position
		camera.position.x += (mouseX - camera.position.x) * positionAdjustment;
		camera.position.y += (-mouseY - camera.position.y) * positionAdjustment;
		// Flag to the particle system that we've changed its vertices. This is
		// the dirty little secret.
		// particleSystem.geometry.__dirtyVertices = true;
		// Render the scene and the camera
		renderer.render(scene, camera);
		// Set up the next call
		requestAnimFrame(animate);
	}

	/**
	 * Track the mouse movements and adjust mouseX and mouseY, in turn updating
	 * the camera angle when render is called.
	 * 
	 * @param {Event}
	 *            e
	 */
	document.addEventListener('mousemove', onDocumentMouseMove, false);

	var mouseX = 0, mouseY = 0;
	var positionAdjustment = 0.02;
	var mouseAdjustment = 1;

	function onDocumentMouseMove(e) {
		mouseX = (e.clientX - windowHalfX) / mouseAdjustment;
		mouseY = (e.clientY - windowHalfY) / mouseAdjustment;
	}

}());
