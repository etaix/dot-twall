if (!Detector.webgl)
	Detector.addGetWebGLMessage();

var container, stats;
var camera, scene, renderer, particles, geometry, materials = [], parameters, i, h, color, sprite, size;
var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

init();
animate();

function init() {

	container = document.createElement('div');
	document.body.appendChild(container);

	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 2000);
	camera.position.z = 1000;

	scene = new THREE.Scene();
	scene.fog = new THREE.FogExp2(0x000000, 0.0008);

	geometry = new THREE.Geometry();

	sprite1 = THREE.ImageUtils.loadTexture("examples/img1.png");
	sprite2 = THREE.ImageUtils.loadTexture("examples/img1.png");
	sprite3 = THREE.ImageUtils.loadTexture("examples/img1.png");
	sprite4 = THREE.ImageUtils.loadTexture("examples/img1.png");
	sprite5 = THREE.ImageUtils.loadTexture("examples/img1.png");

	var colors = [];
	for (i = 0; i < 1000; i++) {
		vector = new THREE.Vector3(Math.random() * 2000 - 1000, Math.random() * 2000 - 1000, Math.random() * 2000 - 1000);
		geometry.vertices.push(new THREE.Vertex(vector));
		colors[i] = new THREE.Color();
		colors[i].setHSV(Math.random(), 1.0, 1.0);
	}
	geometry.colors = colors;
	
	parameters = [ [ [ 1.0, 0.2, 1.0 ], sprite2, 100 ], [ [ 0.95, 0.1, 1 ], sprite3, 150 ], [ [ 0.90, 0.05, 1 ], sprite1, 10 ], [ [ 0.85, 0, 0.8 ], sprite5, 8 ],
			[ [ 0.80, 0, 0.7 ], sprite4, 5 ], ];
	var ind = 0;
	for (i = 0; i < parameters.length; i++) {

		color = parameters[ind][0];
		sprite = parameters[ind][1];
		size = parameters[ind][2];
		ind++;
		if (ind >= parameters.length) {
			ind = 0;
		}
		materials[i] = new THREE.ParticleBasicMaterial({
			size : size,
			map : sprite,
			blending : THREE.AdditiveBlending,
			depthTest : false,
			vertexColors : true,
			transparent : true
		});
//		materials[i].color.setHSV(color[0], color[1], color[2]);

		particles = new THREE.ParticleSystem(geometry, materials[i]);

		particles.rotation.x = Math.random() * 6;
		particles.rotation.y = Math.random() * 6;
		particles.rotation.z = Math.random() * 6;

		scene.add(particles);

	}

	renderer = new THREE.WebGLRenderer({
		clearAlpha : 1
	});
	renderer.setSize(window.innerWidth, window.innerHeight);
	container.appendChild(renderer.domElement);

	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	container.appendChild(stats.domElement);

	document.addEventListener('mousemove', onDocumentMouseMove, false);
	document.addEventListener('touchstart', onDocumentTouchStart, false);
	document.addEventListener('touchmove', onDocumentTouchMove, false);

}

function onDocumentMouseMove(event) {

	mouseX = event.clientX - windowHalfX;
	mouseY = event.clientY - windowHalfY;

}

function onDocumentTouchStart(event) {

	if (event.touches.length == 1) {

		event.preventDefault();

		mouseX = event.touches[0].pageX - windowHalfX;
		mouseY = event.touches[0].pageY - windowHalfY;

	}
}

function onDocumentTouchMove(event) {

	if (event.touches.length == 1) {

		event.preventDefault();

		mouseX = event.touches[0].pageX - windowHalfX;
		mouseY = event.touches[0].pageY - windowHalfY;

	}

}

//

function animate() {

	requestAnimationFrame(animate);

	render();
	stats.update();

}

function render() {

	var time = Date.now() * 0.00005;

	camera.position.x += (mouseX - camera.position.x) * 0.05;
	camera.position.y += (-mouseY - camera.position.y) * 0.05;

	camera.lookAt(scene.position);

	for (i = 0; i < scene.objects.length; i++) {

		scene.objects[i].rotation.y = time * (i < 4 ? i + 1 : -(i + 1));

	}
	var ind = 0;
	for (i = 0; i < materials.length; i++) {

		color = parameters[ind][0];

		h = (360 * (color[0] + time) % 360) / 360;
		materials[ind].color.setHSV(h, color[1], color[2]);
		ind++;
		if (ind >= parameters.length) {
			ind = 0;
		}
	}

	renderer.render(scene, camera);

}
