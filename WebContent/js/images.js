/**
 * Get the pixel color of an image data
 * @param imgData
 * @param x
 * @param y
 * @returns {THREE.Color}
 */
function getPixel(imgData, x, y) {
	var r, g, b, a, offset = x * 4 + y * 4 * imgData.width;
	r = imgData.data[offset];
	g = imgData.data[offset + 1];
	b = imgData.data[offset + 2];
	a = imgData.data[offset + 3];
	return {r: r/255, g: g/255, b: b/255, a: a/255};
}

/**
 * Create a canvas element to draw the image. Then retrieve imagedata
 * @param image
 * @returns
 */
function getImgData(image) {
	var canvas = document.createElement("canvas");
	var context = canvas.getContext("2d");
	canvas.width = image.width;
	canvas.height = image.height;
	context.drawImage(image, 0, 0);
	var imgData = context.getImageData(0, 0, canvas.width, canvas.height);
	return imgData;
}

/**
 * Load an array of image and call the callback when ALL images are loader
 * @param array
 * @param callback
 * @returns {Array}
 */
function loadImageArray(array, callback) {
	var i, l, images = [];
	images.loadCount = 0;
	for( i = 0, l = array.length; i < l; ++i) {

		images[i] = new Image();
		images[i].loaded = 0;
		images[i].onload = function() {
			images.loadCount += 1;
			this.loaded = true;

			if(images.loadCount == l && callback) {
				callback();
			}

		};
		images[i].src = array[i];

	}
	return images;
}