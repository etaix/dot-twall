/**
 * @preserve Main script for Dot-Art experience: http://dot-art.cloudfounrdy.com
 *           This file includes all functionality to render images Depends on
 *           Tween.js, Three.js libraries
 * 
 * Vendor provided code. Compressed file for the live site, this raw script at
 * http://closure-compiler.appspot.com/ using the default, "simple,"
 * optimization setting.
 * 
 * @author eric.taix@gmail.com (Eric Taix)
 */

// ---------------------------------------------------------------------------
// @see http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// ---------------------------------------------------------------------------
window.requestAnimFrame = (function() {
	return window.requestAnimationFrame || window.webkitRequestAnimationFrame
			|| window.mozRequestAnimationFrame || window.oRequestAnimationFrame
			|| window.msRequestAnimationFrame
			|| function(/* function */callback, /* DOMElement */element) {
				window.setTimeout(callback, 1000 / 60);
			};
})();

/**
 * Get window dimensions to save for later so we don't have to query it all the
 * time.
 * 
 * @return {Array.<number>} width and height of window.
 */
function getWindowDimensions() {
	return [ (window.innerWidth || document.documentElement.clientWidth),
			(window.innerHeight || document.documentElement.clientHeight) ];
}

/**
 * Reference to hold window dimensions without having to ask window for them all
 * the time.
 */
var windowDim = getWindowDimensions();

/**
 * Store the center X and Y of the window. Used when adjusting the camera and
 * rotation to make it appear to use the center of the window as the point of
 * reference.
 */
var windowHalfX = windowDim[0] / 2;
var windowHalfY = windowDim[1] / 2;

(function() {

	// Get the DOM element to attach to (assume we've got jQuery to hand)
	var $container = $('#container');
	// Create a WebGL renderer, camera and a scene
	var renderer = new THREE.WebGLRenderer();
	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight,
			0, 2000);
	camera.position.z = 700;
	THREE.WebGLRenderer.addObject(camera, scene);
	// Start the renderer - set the clear colour to whatever color because we
	// also set the opacity to 0.0
	renderer.setClearColor(new THREE.Color(0), 0);
	renderer.setSize(window.innerWidth, window.innerHeight);
	// Attach the render-supplied DOM element
	$container.append(renderer.domElement);

	var started = false;
	var siz = 30;
	var particleSystem = null;
	function test() {
		if (particleSystem != null) {
			renderer.removeObject(particleSystem,scene);
			scene.__objectsRemoved = [];
			scene.__objectsAdded = [];
		}
		
		var texture = "images/plain64.png";
		var material = new THREE.ParticleBasicMaterial({
			color : 0xFF0000,
			size : 30,
			map : THREE.ImageUtils.loadTexture(texture),
			blending : THREE.AdditiveBlending,
			vertexColors : true,
			depthTest : false,
			transparent : false			
		});

		var geometry = new THREE.Geometry();
		var colors = [];
		var dotCount = 0;
		var c = Math.random() * 255;
		for ( var yPos = 0; yPos < 100; yPos++) {
			for ( var xPos = 0; xPos < 100; xPos++) {
				var particle = new THREE.Vertex(new THREE.Vector3(xPos * siz,
						yPos * siz, 0));
				colors[dotCount] = new THREE.Color();
				colors[dotCount].setHSV(c / 255, 1.0, 1.0);
				geometry.vertices.push(particle);
				dotCount++;
			}
		}
		// Change the opacity to transparent
		geometry.colors = colors;
		particleSystem = new THREE.ParticleSystem(geometry, material);
		particleSystem.sortParticles = true;
		scene.addObject(particleSystem);

		// Request the first animation frame
		if (!started) {
			started = true;
			requestAnimFrame(animate);
		}
	}


	/**
	 * Animation loop. This function is called by requestAnimFrame function
	 */
	function animate() {
		// Update all tweens position
		TWEEN.update();
		// Update the camera position
		camera.position.x += (mouseX - camera.position.x) * positionAdjustment;
		camera.position.y += (-mouseY - camera.position.y) * positionAdjustment;
		// Flag to the particle system that we've changed its vertices. This is
		// the dirty little secret.
		// particleSystem.geometry.__dirtyVertices = true;
		// Render the scene and the camera
		
		camera.lookAt( scene.position );
		renderer.render(scene, camera);
		// Set up the next call
		requestAnimFrame(animate);
	}

	/**
	 * Track the mouse movements and adjust mouseX and mouseY, in turn updating
	 * the camera angle when render is called.
	 * 
	 * @param {Event}
	 *            e
	 */
	document.addEventListener('mousemove', onDocumentMouseMove, false);
	document.addEventListener('click', onDocumentClick, false);

	var mouseX = 0, mouseY = 0;
	var positionAdjustment = 0.02;
	var mouseAdjustment = 1;

	function onDocumentMouseMove(e) {
		mouseX = (e.clientX - windowHalfX) / mouseAdjustment;
		mouseY = (e.clientY - windowHalfY) / mouseAdjustment;
	}

	function onDocumentClick(e) {
		test();
	}
}());
